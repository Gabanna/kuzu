package de.rgse.kuzu.importer.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import de.rgse.kuzu.importer.exception.ExcangeException;
import de.rgse.kuzu.model.BooleanTupleEntry;
import de.rgse.kuzu.model.DataSet;
import de.rgse.kuzu.model.FloatTupleEntry;
import de.rgse.kuzu.model.TextTupleEntry;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.TupleEntry;
import de.rgse.kuzu.model.parser.BooleanParser;

public class CSVExchangeService extends HeaderExchangeService {

	private final BooleanParser booleanParser = new BooleanParser();

	private final char delimiter;

	private final boolean hasHeader;

	public CSVExchangeService(String name, String mandant) {
		super(name, mandant);
		this.delimiter = ';';
		this.hasHeader = true;
	}

	public CSVExchangeService(String name, String mandant, char delimiter) {
		super(name, mandant);
		this.delimiter = delimiter;
		this.hasHeader = true;
	}

	public CSVExchangeService(String name, String mandant, char delimiter, boolean hasHeader) {
		super(name, mandant);
		this.delimiter = delimiter;
		this.hasHeader = hasHeader;
	}

	@Override
	public DataSet importData(File file) throws ExcangeException {
		if (null != file && file.exists()) {
			try {
				final CSVParser csvParser = createCSVParser(file);
				final List<CSVRecord> records = csvParser.getRecords();
				csvParser.close();

				return createDataSet(records);

			} catch (final IOException e) {
				throw new ExcangeException(e);
			}

		} else {
			throw getNoFileException();
		}
	}

	@Override
	public File exportData(DataSet dataSet) throws ExcangeException {
		if (null != dataSet) {
			try {
				final File file = File
						.createTempFile("export_" + dataSet.getMandant() + "_" + dataSet.getName(), "csv");

				final FileWriter fileWriter = new FileWriter(file);
				final CSVPrinter csvPrinter = new CSVPrinter(fileWriter, createFormat());

				if (hasHeader) {
					final Tuple next = dataSet.getData().iterator().next();
					csvPrinter.printRecord(next.getCriteria());
				}

				for (final Tuple tuple : dataSet.getData()) {
					final Object[] data = new Object[tuple.size()];

					int i = 0;
					for (final TupleEntry<?> entry : tuple.getEntries()) {
						data[i++] = entry.getValue();
					}

					csvPrinter.printRecord(data);
				}

				csvPrinter.close();

				return file;

			} catch (final IOException e) {
				throw new ExcangeException(e);
			}

		} else {
			throw getNoFileException();
		}
	}

	private Tuple createTuple(CSVRecord record, String[] header) {
		final Tuple tuple = new Tuple();

		for (int i = 0; i < record.size(); i++) {
			TupleEntry<?> tupleEntry;
			try {
				final float value = Float.valueOf(record.get(i));
				tupleEntry = new FloatTupleEntry(header[i], value);

			} catch (final NumberFormatException exception) {
				final String value = record.get(i);

				if (null != value && booleanParser.isBoolean(value)) {
					final boolean bvalue = booleanParser.parse(record.get(i));
					tupleEntry = new BooleanTupleEntry(header[i], bvalue);

				} else if (null != value) {
					tupleEntry = new TextTupleEntry(header[i], value);

				} else {
					tupleEntry = new TextTupleEntry(header[i]);

				}

			}

			tuple.addEntry(tupleEntry);
		}

		return tuple;
	}

	private CSVParser createCSVParser(File file) throws IOException, FileNotFoundException {
		return new CSVParser(new FileReader(file), createFormat());
	}

	private CSVFormat createFormat() {
		return CSVFormat.DEFAULT.withRecordSeparator("\n").withDelimiter(delimiter);
	}

	private DataSet createDataSet(List<CSVRecord> records) {
		final DataSet dataSet = new DataSet(getName(), getMandant());
		final List<CSVRecord> list = hasHeader ? records.subList(1, records.size() - 1) : records;

		final String[] header = createHeaderFromCSV(records.get(0));

		for (final CSVRecord csvRecord : list) {
			dataSet.addTuple(createTuple(csvRecord, header));
		}

		return dataSet;
	}

	private String[] createHeaderFromCSV(CSVRecord record) {
		final Object[] elements = new Object[record.size()];
		for (int i = 0; i < record.size(); i++) {
			elements[i] = record.get(i);
		}
		return createHeader(elements);
	}

}
