package de.rgse.kuzu.importer.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.rgse.kuzu.exception.NotCalculableException;
import de.rgse.kuzu.importer.exception.ExcangeException;
import de.rgse.kuzu.model.BooleanTupleEntry;
import de.rgse.kuzu.model.DataSet;
import de.rgse.kuzu.model.FloatTupleEntry;
import de.rgse.kuzu.model.TextTupleEntry;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.TupleEntry;
import de.rgse.kuzu.model.datasetview.BooleanDataSetView;
import de.rgse.kuzu.model.datasetview.NumericDataSetView;
import de.rgse.kuzu.model.parser.BooleanParser;

public class ExcelExchangeService extends HeaderExchangeService {

	private final BooleanParser booleanParser = new BooleanParser();

	public static void main(String[] args) throws ExcangeException, NotCalculableException {
		final ExcelExchangeService excelExchangeService = new ExcelExchangeService("test", "12345");
		final DataSet importedData = excelExchangeService.importData(new File("H:\\umfrage.xlsx"));

		System.out.println(importedData.getMandant() + "@" + importedData.getName());
		System.out.println();

		final NumericDataSetView numericView = importedData.getNumericView("Frage 1");
		System.out.println("avg:" + numericView.getAvg());
		System.out.println("count:" + numericView.getCount());
		System.out.println("max:" + numericView.getMax());
		System.out.println("min:" + numericView.getMin());

		final BooleanDataSetView booleanView = importedData.getBooleanView("Frage 4");
		System.out.println("count:" + booleanView.getCount());
		System.out.println("true:" + booleanView.getTrueCount());
		System.out.println("true in %:" + booleanView.getTrueInPercent());

	}

	public ExcelExchangeService(String name, String mandant) {
		super(name, mandant);
	}

	@Override
	public DataSet importData(File file) throws ExcangeException {
		if (null != file && file.exists()) {
			try {
				final FileInputStream fileInputStream = new FileInputStream(file);
				final XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
				final DataSet createDataSet = createDataSet(workbook.getSheetAt(0), getName());
				workbook.close();

				return createDataSet;

			} catch (final IOException exception) {
				throw new ExcangeException(exception);
			}

		} else {
			throw getNoFileException();
		}
	}

	public DataSet[] importDatas(File file) throws ExcangeException {
		if (null != file && file.exists()) {
			try {
				final FileInputStream fileInputStream = new FileInputStream(file);
				final XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);

				final DataSet[] dataSets = new DataSet[workbook.getNumberOfSheets()];

				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
					final Sheet sheet = workbook.getSheetAt(i);
					dataSets[i] = createDataSet(sheet);
				}

				workbook.close();
				return dataSets;

			} catch (final IOException biffException) {
				throw new ExcangeException(biffException);
			}

		} else {
			throw getNoFileException();
		}
	}

	@Override
	public File exportData(DataSet dataSet) throws ExcangeException {
		if (null != dataSet) {
			try {
				final XSSFWorkbook workbook = new XSSFWorkbook();
				printDataSet(dataSet, workbook);

				final File file = File.createTempFile("export_" + dataSet.getMandant() + "_" + dataSet.getName(),
						"xlsx");
				workbook.write(new FileOutputStream(file));
				workbook.close();

				return file;

			} catch (final IOException exception) {
				throw new ExcangeException(exception);
			}

		} else {
			throw getNoFileException();
		}
	}

	public File exportDatas(DataSet[] dataSet) throws ExcangeException {
		try {
			final XSSFWorkbook workbook = new XSSFWorkbook();

			for (final DataSet set : dataSet) {
				printDataSet(set, workbook);
			}

			final File file = File.createTempFile("export_" + getMandant(), "xlsx");
			workbook.write(new FileOutputStream(file));
			workbook.close();

			return file;

		} catch (final IOException exception) {
			throw new ExcangeException(exception);
		}
	}

	private void printDataSet(DataSet dataSet, XSSFWorkbook workbook) {
		final Sheet sheet = workbook.createSheet(dataSet.getName());
		printHeader(sheet, dataSet.getAvailableCriteria());
		printContent(sheet, dataSet);
	}

	private void printHeader(Sheet sheet, Collection<String> availableCriteria) {
		final List<String> header = new LinkedList<>(availableCriteria);
		final Row headerRow = sheet.createRow(1);
		for (int i = 0; i < availableCriteria.size(); i++) {
			final Cell cell = headerRow.createCell(i);
			cell.setCellValue(header.get(i));
		}
	}

	private void printContent(Sheet sheet, DataSet dataSet) {
		final List<Tuple> tuples = new LinkedList<>(dataSet.getData());

		for (int i = 0; i < tuples.size(); i++) {
			final Row row = sheet.createRow(i + 1);
			final Tuple tuple = tuples.get(i);
			final List<TupleEntry<?>> entries = new LinkedList<>(tuple.getEntries());

			for (int j = 0; j < entries.size(); j++) {
				final TupleEntry<?> tupleEntry = entries.get(j);
				if (tupleEntry.isNumeric()) {
					row.createCell(j).setCellValue((Float) tupleEntry.getValue());

				} else if (tupleEntry.isBoolean()) {
					row.createCell(j).setCellValue((Boolean) entries.get(j).getValue());

				} else {
					row.createCell(j).setCellValue(entries.get(j).getValue().toString());

				}
			}
		}
	}

	private DataSet createDataSet(Sheet sheet, String name) {
		final DataSet dataSet = new DataSet(name, getMandant());

		final String[] headers = createHeaderFromXLS(sheet);

		for (int i = 0; i <= sheet.getLastRowNum(); i++) {
			final Row row = sheet.getRow(i + 1);
			if (null != row) {
				final Tuple tuple = createTuple(row, headers);
				dataSet.addTuple(tuple);
			}
		}

		return dataSet;
	}

	private DataSet createDataSet(Sheet sheet) {
		return createDataSet(sheet, sheet.getSheetName());
	}

	private String[] createHeaderFromXLS(Sheet sheet) {
		final Row row = sheet.getRow(0);
		Object[] headers;
		if (null != row) {
			headers = new Object[row.getLastCellNum()];

			for (int i = 0; i < row.getLastCellNum(); i++) {
				final Cell cell = row.getCell(i);
				if (null != cell) {
					headers[i] = cell.getStringCellValue();
				}
			}
		} else {
			headers = new Object[0];
		}

		return createHeader(headers);
	}

	private Tuple createTuple(Row row, String[] header) {
		final Tuple tuple = new Tuple();

		for (int i = 0; i < row.getLastCellNum(); i++) {
			final Cell cell = row.getCell(i);
			TupleEntry<?> tupleEntry = null;
			final String criterion = header[i];

			if (null != cell) {
				tupleEntry = fillTupleEntry(cell, criterion);

			} else {
				tupleEntry = new TextTupleEntry(criterion);
			}

			tuple.addEntry(tupleEntry);
		}

		return tuple;
	}

	private TupleEntry<?> fillTupleEntry(final Cell cell, final String criterion) {
		TupleEntry<?> tupleEntry;
		if (Cell.CELL_TYPE_NUMERIC == cell.getCellType() || Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
			tupleEntry = new FloatTupleEntry(criterion, Double.valueOf(cell.getNumericCellValue()).floatValue());

		} else if (Cell.CELL_TYPE_BOOLEAN == cell.getCellType()) {
			tupleEntry = new BooleanTupleEntry(criterion, cell.getBooleanCellValue());

		} else {
			final String stringCellValue = cell.getStringCellValue();

			if (booleanParser.isBoolean(stringCellValue)) {
				tupleEntry = new BooleanTupleEntry(criterion, booleanParser.parse(stringCellValue));

			} else {
				tupleEntry = new TextTupleEntry(criterion, stringCellValue);

			}
		}
		return tupleEntry;
	}
}
