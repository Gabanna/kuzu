package de.rgse.kuzu.importer.exception;

@SuppressWarnings("serial")
public class ExcangeException extends Exception {

	public ExcangeException(Throwable cause) {
		super(cause);
	}

	public ExcangeException(String message, Throwable cause) {
		super(message, cause);
	}
}
