package de.rgse.kuzu.importer.service;

public abstract class HeaderExchangeService extends ExchangeService {

	private final boolean hasHeader;

	public HeaderExchangeService(String name, String mandant, boolean hasHeader) {
		super(name, mandant);
		this.hasHeader = hasHeader;
	}

	public HeaderExchangeService(String name, String mandant) {
		super(name, mandant);
		this.hasHeader = true;
	}

	protected String[] createHeader(Object... record) {
		final String[] header = new String[record.length];

		if (hasHeader && 0 < record.length) {
			for (int i = 0; i < record.length; i++) {
				header[i] = record[i].toString();
			}

		} else {
			final int charA = Character.getNumericValue('A');
			for (int i = 0; i < header.length; i++) {
				header[i] = String.valueOf((char) (i + charA));
			}
		}

		return header;
	}

	public boolean hasHeader() {
		return hasHeader;
	}
}
