package de.rgse.kuzu.importer.service;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.opendatafoundation.data.spss.SPSSFile;
import org.opendatafoundation.data.spss.SPSSFileException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.rgse.kuzu.importer.exception.ExcangeException;
import de.rgse.kuzu.model.DataSet;

public class SPSSExchangeService extends ExchangeService {

	public static void main(String[] args) throws ExcangeException {
		final File file = new File("H:\\Robbery_Incident.sav");
		final SPSSExchangeService excelExchangeSession = new SPSSExchangeService("test", "543");
		final DataSet importData = excelExchangeSession.importData(file);
	}

	public SPSSExchangeService(String name, String mandant) {
		super(name, mandant);
	}

	@Override
	public DataSet importData(File file) throws ExcangeException {
		if (null != file && file.exists()) {
			try {
				final SPSSFile spssFile = new SPSSFile(file);
				spssFile.loadMetadata();
				spssFile.loadData();

				final Document document = spssFile.getDDI2();
				final NodeList elementsByTagName = document.getElementsByTagName("Name");

				for (int i = 0; i < elementsByTagName.getLength(); i++) {
					final Node item = elementsByTagName.item(i);
					System.out.println(item.getNodeName());
				}

				final TransformerFactory transfac = TransformerFactory.newInstance();
				final Transformer trans = transfac.newTransformer();
				trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
				trans.setOutputProperty(OutputKeys.INDENT, "yes");
				trans.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, "yes");

				final StringWriter sw = new StringWriter();
				final StreamResult result = new StreamResult(sw);
				final DOMSource source = new DOMSource(document);
				trans.transform(source, result);
				final String xmlString = sw.toString();

				System.out.println("!!!");
				System.out.println(xmlString);

				return null;
			} catch (final SPSSFileException | IOException | TransformerException exception) {
				throw new ExcangeException(exception);
			}

		} else {
			throw getNoFileException();
		}
	}

	@Override
	public File exportData(DataSet dataSet) throws ExcangeException {
		return null;
	}

}
