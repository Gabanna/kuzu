package de.rgse.kuzu.importer.service;

import java.io.File;

import de.rgse.kuzu.importer.exception.ExcangeException;
import de.rgse.kuzu.model.DataSet;

public abstract class ExchangeService {

	private final String name;

	private final String mandant;

	public ExchangeService(String name, String mandant) {
		this.name = name;
		this.mandant = mandant;
	}

	public abstract DataSet importData(File file) throws ExcangeException;

	public abstract File exportData(DataSet dataSet) throws ExcangeException;

	public String getMandant() {
		return mandant;
	}

	public String getName() {
		return name;
	}

	protected ExcangeException getNoFileException() {
		return new ExcangeException(new IllegalArgumentException("There could be no file found for reading"));
	}
}
