package de.rgse.test.expression.model;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import de.rgse.kuzu.calculator.exception.NoDataException;
import de.rgse.kuzu.calculator.model.Operation;
import de.rgse.kuzu.exception.NotCalculableException;
import de.rgse.kuzu.model.DataSet;
import de.rgse.kuzu.model.FloatTupleEntry;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.datasetview.NumericDataSetView;

public class OperationTest {

	@Test
	public void calculateCountTest() throws NoDataException {
		Operation operation = new Operation("count()");

		final NumericDataSetView createCalculateCountMock = createCalculateCountMock();

		Double value = operation.calculate(createCalculateCountMock);
		Assert.assertEquals(10, value, 0);

		operation = new Operation("count()");
		value = operation.calculate(createCalculateCountMock);
		Assert.assertEquals(10, value, 0);
	}

	@Test
	public void calculateSumTest() throws NoDataException, NotCalculableException {
		final DataSet dataSet = createCalculateSumMock();

		Operation operation = new Operation("sum()");
		Double value = operation.calculate(dataSet.getNumericView("A"));
		Assert.assertEquals(9, value, 0);

		operation = new Operation("sum(B)");
		value = operation.calculate(dataSet.getNumericView("B"));
		Assert.assertEquals(8, value, 0);

		operation = new Operation("sum(A)");
		value = operation.calculate(dataSet.getNumericView("B"));
		Assert.assertEquals(9, value, 0);
	}

	private NumericDataSetView createCalculateCountMock() {
		@SuppressWarnings("unchecked")
		final Collection<Float> tuples = Mockito.mock(Collection.class);
		Mockito.when(tuples.size()).thenReturn(10);

		final NumericDataSetView mock = Mockito.mock(NumericDataSetView.class);
		Mockito.when(mock.getAll()).thenReturn(tuples);

		return mock;
	}

	private DataSet createCalculateSumMock() {
		final DataSet dataSet = new DataSet("Test-DataSet", "Testsystem");
		Tuple tuple = new Tuple();
		tuple.addEntry(new FloatTupleEntry("A", 5));
		tuple.addEntry(new FloatTupleEntry("B", 4));
		dataSet.addTuple(tuple);

		tuple = new Tuple();
		tuple.addEntry(new FloatTupleEntry("A", 4));
		tuple.addEntry(new FloatTupleEntry("B", 4));
		dataSet.addTuple(tuple);

		return dataSet;
	}
}
