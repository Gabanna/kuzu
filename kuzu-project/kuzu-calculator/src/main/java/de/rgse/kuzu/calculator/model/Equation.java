package de.rgse.kuzu.calculator.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;

import de.rgse.kuzu.calculator.exception.NoDataException;
import de.rgse.kuzu.calculator.service.ExtendedDoubleEvaluator;
import de.rgse.kuzu.model.datasetview.NumericDataSetView;

public class Equation extends ArithmeticElement {

	private final ExtendedDoubleEvaluator doubleEvaluator;
	private final String equation;
	private final Collection<Operation> operations;

	public Equation(String equation) {
		doubleEvaluator = new ExtendedDoubleEvaluator();
		this.equation = new String(equation);
		operations = analyse(equation);
	}

	public Float calculate(NumericDataSetView dataSetView) throws NoDataException {
		String calcTerm = new String(equation);

		for (final Operation operation : operations) {
			calcTerm = calcTerm.replace(operation.toString(), new BigDecimal(operation.calculate(dataSetView))
			.setScale(3, RoundingMode.HALF_EVEN).toPlainString());
		}

		final Double evaluate = doubleEvaluator.evaluate(calcTerm);
		return evaluate.floatValue();
	}

	@Override
	public String toString() {
		return equation;
	}

}
