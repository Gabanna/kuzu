package de.rgse.kuzu.calculator.model;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.rgse.kuzu.calculator.exception.NoDataException;
import de.rgse.kuzu.calculator.service.ExtendedDoubleEvaluator;
import de.rgse.kuzu.calculator.util.ResultFormatter;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.datasetview.NumericDataSetView;
import de.rgse.kuzu.model.util.StringUtil;

public class Operation extends ArithmeticElement {

	private final ExtendedDoubleEvaluator doubleEvaluator;
	private final ArithmeticOperation operation;
	private final String term;
	private final Collection<Operation> operations;

	public Operation(String equation) {
		doubleEvaluator = new ExtendedDoubleEvaluator();
		operation = ArithmeticOperation.parse(equation.substring(0, equation.indexOf("(")));
		term = equation.substring(equation.indexOf("("));
		operations = analyse(term);
	}

	@Override
	public String toString() {
		return operation.getId() + term;
	}

	/**
	 * Returns the result of the defined Operation. IF there are nested
	 * Operations all of them are getting calculated.
	 *
	 * @param dataSetView
	 *            containing the relevant data
	 * @return the calculated value
	 * @throws NoDataException
	 *             if the given dataSet is empty or null
	 */
	public Double calculate(NumericDataSetView dataSetView) throws NoDataException {
		Double result = null;

		if (term.contains("{")) {
			final Collection<Float> tempResult = calculateInnerEquation(dataSetView);
			result = operation.calculate(tempResult).doubleValue();

		} else {
			final String contentWithinBrackets = StringUtil.getContentWithinBrackets(term);
			final Collection<Float> tempResult = null == contentWithinBrackets ? dataSetView.getAll() : dataSetView
					.getAll(contentWithinBrackets);
			result = operation.calculate(tempResult).doubleValue();
		}

		return result;
	}

	private Collection<Float> calculateInnerEquation(NumericDataSetView dataSetView) throws NoDataException {
		final Collection<Float> tempResult = new ArrayDeque<Float>();
		for (final Tuple tuple : dataSetView.getData()) {
			String calcTerm = replaceColumnValues(tuple);
			calcTerm = replaceOperations(dataSetView, calcTerm);
			final Double evaluate = doubleEvaluator.evaluate(calcTerm);
			tempResult.add(evaluate.floatValue());
		}
		return tempResult;
	}

	private String replaceOperations(NumericDataSetView dataSetView, String calcTerm) throws NoDataException {
		for (final Operation operation : operations) {
			calcTerm = calcTerm.replace(operation.toString(),
					ResultFormatter.formatResult(operation.calculate(dataSetView)));
		}
		return calcTerm;
	}

	private String replaceColumnValues(final Tuple tuple) {
		String calcTerm = new String(term);
		for (final String columnName : getColumns()) {
			calcTerm = calcTerm.replace("{" + columnName + "}",
					ResultFormatter.format(tuple.get(columnName.toUpperCase()).toString()));
		}
		return calcTerm;
	}

	private Set<String> getColumns() {
		final Pattern p = Pattern.compile("\\{(.*?)\\}");
		final Matcher m = p.matcher(term);
		final Set<String> columns = new HashSet<String>();

		while (m.find()) {
			columns.add(m.group(1));
		}

		return columns;
	}
}
