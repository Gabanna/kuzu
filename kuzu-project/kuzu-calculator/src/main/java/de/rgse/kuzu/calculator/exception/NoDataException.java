package de.rgse.kuzu.calculator.exception;

@SuppressWarnings("serial")
public class NoDataException extends Exception {

	public NoDataException() {
		super(
				"Es liegen keine Daten zur Berechnung vor.\nEntweder ist das DataSet leer, oder es gibt keine Daten mit den angegebenen Filterkriterien.");
	}
}
