package de.rgse.kuzu.calculator.model;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;

import de.rgse.kuzu.calculator.exception.NoDataException;
import de.rgse.kuzu.model.util.StringUtil;

public enum ArithmeticOperation {

	COUNT(t -> (float) t.size()), MIN(t -> (float) Collections.min(t)), MAX(t -> (float) Collections.max(t)), AVG(
			t -> {
				float sum = 0;
				for (final Float f : t) {
					sum += f;
				}
				return sum / t.size();

			}), SUM(t -> {
				float sum = 0;
				for (final Float f : t) {
					sum += f;
				}
				return sum;
			});

	private String id;

	private Function<Collection<Float>, Float> operation;

	private ArithmeticOperation(Function<Collection<Float>, Float> operation) {
		this.id = name().toLowerCase();
		this.operation = operation;
	}

	private ArithmeticOperation(String id, Function<Collection<Float>, Float> operation) {
		this.id = id;
		this.operation = operation;
	}

	public String getId() {
		return id;
	}

	public Float calculate(Collection<Float> floats) throws NoDataException {

		if (null != floats && !floats.isEmpty()) {
			return operation.apply(floats);

		} else {
			throw new NoDataException();
		}

	}

	public Operation getOperation(String text) {
		Operation operation = null;

		if (text.contains(id)) {

			final String[] split = text.split("(?=" + id + ")");

			String op = null;

			for (final String string : split) {
				if (string.startsWith(id)) {
					op = string;
					break;
				}
			}

			op = op.substring(0, StringUtil.findClosingParen(op) + 1);
			operation = new Operation(op);
		}
		return operation;
	}

	public static ArithmeticOperation parse(String toParse) {
		ArithmeticOperation result = null;

		for (final ArithmeticOperation operation : ArithmeticOperation.values()) {
			if (operation.getId().equals(toParse)) {
				result = operation;
				break;
			}
		}

		return result;
	}

	public static ArithmeticOperation isEndingOf(String text) {
		ArithmeticOperation result = null;

		for (final ArithmeticOperation operation : ArithmeticOperation.values()) {
			if (text.endsWith(operation.getId())) {
				result = operation;
				break;
			}
		}

		return result;
	}

}
