package de.rgse.kuzu.calculator.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ResultFormatter {

	public static String format(String result) {
		return formatResult(new BigDecimal(result));
	}

	public static String formatResult(Double result) {
		return formatResult(new BigDecimal(result));
	}

	private static String formatResult(BigDecimal result) {
		return result.setScale(4, RoundingMode.HALF_EVEN).toPlainString();
	}
}
