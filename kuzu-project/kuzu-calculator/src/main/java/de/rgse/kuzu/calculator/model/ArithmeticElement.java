package de.rgse.kuzu.calculator.model;

import java.util.ArrayDeque;
import java.util.Collection;

import de.rgse.kuzu.model.util.StringUtil;

public class ArithmeticElement {

	protected Collection<Operation> analyse(String text) {
		final Collection<Operation> operations = new ArrayDeque<Operation>();

		String expression = new String(text);
		while (expression.length() > 0) {
			int indexOf = expression.indexOf("(");
			ArithmeticOperation operation = null;
			if (0 < indexOf) {
				final String part = expression.substring(0, indexOf);
				operation = ArithmeticOperation.isEndingOf(part);

			} else {
				indexOf = 0;
			}

			if (null != operation) {
				operations.add(operation.getOperation(expression));
				expression = expression.substring(expression.indexOf("("));
				expression = expression.substring(StringUtil.findClosingParen(expression) + 1);

			} else {
				expression = expression.substring(indexOf + 1);
			}

		}

		return operations;
	}
}
