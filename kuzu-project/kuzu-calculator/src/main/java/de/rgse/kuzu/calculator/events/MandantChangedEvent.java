package de.rgse.kuzu.calculator.events;

public class MandantChangedEvent {

	private final String mandant;

	public MandantChangedEvent(String mandant) {
		this.mandant = mandant;
	}

	public String getMandant() {
		return mandant;
	}
}
