package de.rgse.kuzu.model.filter;

import java.util.ArrayDeque;
import java.util.Collection;

import de.rgse.kuzu.exception.IncompatibleDatatypeException;
import de.rgse.kuzu.exception.NotCalculableException;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.datasetview.NumericDataSetView;

public class PikDataSetFilter implements DataSetFilter {

	private final float standardDeviation;
	private final float average;

	public PikDataSetFilter(NumericDataSetView dataSetView) throws NotCalculableException {
		standardDeviation = dataSetView.getStandardDeviation();
		average = dataSetView.getAvg();
	}

	@Override
	public Collection<Tuple> filter(Collection<Tuple> data, String criterion) {
		final Collection<Tuple> result = new ArrayDeque<Tuple>();

		for (final Tuple tuple : data) {
			try {
				final Float value = tuple.getAsFloat(criterion);

				if (average - standardDeviation > value || average + standardDeviation < value) {
					result.add(tuple);
				}

			} catch (final IncompatibleDatatypeException datatypeException) {
				// TODO
			}
		}

		System.out.println(getClass().getSimpleName() + ": input=" + data.size() + "; output=" + result.size());

		return result;
	}

}
