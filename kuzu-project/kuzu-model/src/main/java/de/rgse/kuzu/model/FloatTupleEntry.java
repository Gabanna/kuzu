package de.rgse.kuzu.model;

import de.rgse.kuzu.model.parser.FloatParser;

public class FloatTupleEntry extends TupleEntry<Float> {

	private static FloatParser floatParser = new FloatParser();

	public FloatTupleEntry(String criterion, Integer value) {
		super(criterion, value.floatValue());
	}

	public FloatTupleEntry(String criterion, Float value) {
		super(criterion, value);
	}

	public FloatTupleEntry(String criterion) {
		super(criterion);
	}

	@Override
	public void setValue(String value) {
		setValue(floatParser.parse(value));
	}

}
