package de.rgse.kuzu.model.datasetview;

import de.rgse.kuzu.exception.IncompatibleDatatypeException;
import de.rgse.kuzu.exception.NotCalculableException;
import de.rgse.kuzu.model.DataSet;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.filter.DataSetFilter;

public class BooleanDataSetView extends DataSetView<Boolean> {

	public BooleanDataSetView(DataSet dataSet, String criterion, DataSetFilter[] filters) {
		super(dataSet, criterion, filters);
	}

	public float getTrueCount() throws NotCalculableException {
		try {
			float count = 0;

			for (final Tuple tuple : getData()) {
				if (tuple.getAsBoolean(getCriterion())) {
					count++;
				}
			}

			return count;

		} catch (final IncompatibleDatatypeException datatypeException) {
			throw new NotCalculableException(datatypeException);
		}
	}

	public float getTrueInPercent() throws NotCalculableException {
		return getTrueCount() / getCount();
	}
}
