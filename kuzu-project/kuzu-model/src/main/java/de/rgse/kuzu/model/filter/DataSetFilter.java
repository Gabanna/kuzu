package de.rgse.kuzu.model.filter;

import java.util.Collection;

import de.rgse.kuzu.model.Tuple;

public interface DataSetFilter {

	Collection<Tuple> filter(Collection<Tuple> data, String criterion);
}
