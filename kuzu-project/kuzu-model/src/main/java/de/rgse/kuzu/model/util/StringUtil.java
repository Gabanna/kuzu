package de.rgse.kuzu.model.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	public static int findClosingParen(String text) {
		int closePos = text.indexOf('(');
		int counter = 1;
		final char[] chars = text.toCharArray();

		while (counter > 0) {
			final char c = chars[++closePos];

			if (c == '(') {
				counter++;

			} else if (c == ')') {
				counter--;
			}
		}

		return closePos;
	}

	public static int findOpenParen(String text) {
		int openPos = text.lastIndexOf(')');
		int counter = 1;
		final char[] chars = text.toCharArray();

		while (counter > 0) {
			final char c = chars[--openPos];
			if (c == '(') {
				counter--;
			} else if (c == ')') {
				counter++;
			}
		}

		return openPos;
	}

	public static String getContentWithinBrackets(String text) {
		final Pattern p = Pattern.compile("\\(([^)]+)\\)");
		final Matcher m = p.matcher(text);

		String result = null;
		if (m.find()) {
			result = m.group(1);
		}

		return result;
	}
}
