package de.rgse.kuzu.model.datasetview;

import de.rgse.kuzu.exception.IncompatibleDatatypeException;
import de.rgse.kuzu.exception.NotCalculableException;
import de.rgse.kuzu.model.DataSet;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.filter.DataSetFilter;

public class NumericDataSetView extends DataSetView<Float> {

	public NumericDataSetView(DataSet dataSet, String criterion, DataSetFilter[] filters) {
		super(dataSet, criterion, filters);
	}

	public float getAvg() throws NotCalculableException {
		return getSum() / getCount();
	}

	public float getVariance() throws NotCalculableException {
		try {
			final float avg = getAvg();

			float variance = 0;
			for (final Tuple tuple : getData()) {
				final float val = tuple.get(getCriterion(), Float.class) - avg;
				variance += val * val;
			}

			return variance / getCount();

		} catch (final IncompatibleDatatypeException datatypeException) {
			throw new NotCalculableException(datatypeException);
		}
	}

	public float getStandardDeviation() throws NotCalculableException {
		return (float) Math.sqrt(getVariance());
	}

	public float getSum() throws NotCalculableException {
		try {
			float result = 0;

			for (final Tuple tuple : getData()) {
				final Float criterionValue = tuple.get(getCriterion(), Float.class);

				if (null != criterionValue) {
					result += tuple.get(getCriterion(), Float.class);
				}
			}

			return result;

		} catch (final IncompatibleDatatypeException datatypeException) {
			throw new NotCalculableException(datatypeException);
		}
	}

	public float getMin() throws NotCalculableException {
		try {
			float min = 0;

			for (final Tuple tuple : getData()) {
				final Float f = tuple.get(getCriterion(), Float.class);
				min = null != f && min > f ? f : min;
			}

			return min;

		} catch (final IncompatibleDatatypeException datatypeException) {
			throw new NotCalculableException(datatypeException);
		}
	}

	public float getMax() throws NotCalculableException {
		try {
			float max = 0;
			for (final Tuple tuple : getData()) {
				final Float f = tuple.get(getCriterion(), Float.class);
				max = null != f && max < f ? f : max;
			}

			return max;

		} catch (final IncompatibleDatatypeException datatypeException) {
			throw new NotCalculableException(datatypeException);
		}
	}
}
