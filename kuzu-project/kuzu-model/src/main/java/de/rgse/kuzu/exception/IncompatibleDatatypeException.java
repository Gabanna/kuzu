package de.rgse.kuzu.exception;

@SuppressWarnings("serial")
public class IncompatibleDatatypeException extends Exception {

	private Class<?> target;
	private Class<?> source;

	public IncompatibleDatatypeException(Class<?> targetType, Class<?> sourceType) {
		this.target = targetType;
		this.source = sourceType;
	}

	public IncompatibleDatatypeException(ClassCastException exception) {
		super(exception);
	}

	public Class<?> getSource() {
		return source;
	}

	public Class<?> getTarget() {
		return target;
	}
}
