package de.rgse.kuzu.model;

public abstract class TupleEntry<T> {

	private final String name;

	private T value;

	public TupleEntry(String criterion, T value) {
		super();
		this.name = criterion;
		setValue(value);
	}

	public TupleEntry(String criterion) {
		super();
		this.name = criterion;
		setValue((T) null);
	}

	public abstract void setValue(String value);

	public String getName() {
		return name;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public boolean isNumeric() {
		return getValue() instanceof Float;
	}

	public boolean isBoolean() {
		return getValue() instanceof Boolean;
	}

	@Override
	public String toString() {
		return name + ":" + value;
	}
}
