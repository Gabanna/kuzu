package de.rgse.kuzu.model.parser;

public class FloatParser extends TupleValueParser<Float> {

	@Override
	public Float parse(String input) {
		Float result = null;
		if (null != input) {
			try {
				result = Float.valueOf(input);

			} catch (final NumberFormatException exception) {
				result = Float.NaN;
			}
		}

		return result;
	}
}
