package de.rgse.kuzu.exception;

@SuppressWarnings("serial")
public class NotCalculableException extends Exception {

	public NotCalculableException(Throwable cause) {
		super(cause);
	}
}
