package de.rgse.kuzu.model;

public class TextTupleEntry extends TupleEntry<String> {

	public TextTupleEntry(String criterion, String value) {
		super(criterion, value);
	}

	public TextTupleEntry(String criterion) {
		super(criterion);
	}

}
