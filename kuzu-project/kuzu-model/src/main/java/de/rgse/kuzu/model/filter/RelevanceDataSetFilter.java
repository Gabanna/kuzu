package de.rgse.kuzu.model.filter;

import java.util.ArrayDeque;
import java.util.Collection;

import de.rgse.kuzu.model.Tuple;

public class RelevanceDataSetFilter implements DataSetFilter {

	@Override
	public Collection<Tuple> filter(Collection<Tuple> data, String criterion) {
		final Collection<Tuple> result = new ArrayDeque<Tuple>();

		for (final Tuple tuple : data) {
			if (tuple.isRelevant()) {
				result.add(tuple);
			}
		}

		return result;
	}

}
