package de.rgse.kuzu.model.parser;

public class BooleanParser extends TupleValueParser<Boolean> {

	@Override
	public Boolean parse(String input) {
		Boolean result = null;

		if (null != input && isBoolean(input)) {
			result = isTrue(input);
		}

		return result;
	}

	public boolean isBoolean(String input) {
		return isTrue(input) || isFalse(input);
	}

	public boolean isTrue(String input) {
		return input.equals("true") || input.equals("ja") || input.equals("1") || input.equals("j")
				|| input.equals("y");
	}

	public boolean isFalse(String input) {
		return input.equals("false") || input.equals("nein") || input.equals("0") || input.equals("n");
	}
}
