package de.rgse.kuzu.model;

import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import de.rgse.kuzu.exception.IncompatibleDatatypeException;
import de.rgse.kuzu.exception.NotCalculableException;
import de.rgse.kuzu.model.datasetview.BooleanDataSetView;
import de.rgse.kuzu.model.datasetview.NumericDataSetView;
import de.rgse.kuzu.model.datasetview.TextDataSetView;
import de.rgse.kuzu.model.filter.DataSetFilter;

public class DataSet {

	private final String name;

	private final String mandant;

	private final Collection<Tuple> data;

	private final Collection<String> criterionBlacklist;

	public DataSet(String name, String mandant) {
		this.name = name;
		this.mandant = mandant;
		data = new ArrayDeque<Tuple>();
		criterionBlacklist = new ArrayDeque<String>();
	}

	public void addTuple(Tuple tuple) {
		data.add(tuple);
	}

	public Collection<Tuple> getData() {
		return data;
	}

	public TextDataSetView getTextView(String criterion, DataSetFilter... filter) {
		return new TextDataSetView(this, criterion, filter);
	}

	public BooleanDataSetView getBooleanView(String criterion, DataSetFilter... filter) throws NotCalculableException {
		try {
			final Tuple next = data.iterator().next();

			next.getAsBoolean(criterion);
			return new BooleanDataSetView(this, criterion, filter);

		} catch (final IncompatibleDatatypeException datatypeException) {
			throw new NotCalculableException(datatypeException);
		}
	}

	public NumericDataSetView getNumericView(String criterion, DataSetFilter... filter) throws NotCalculableException {
		try {
			final Tuple next = data.iterator().next();

			next.getAsFloat(criterion);
			return new NumericDataSetView(this, criterion, filter);

		} catch (final IncompatibleDatatypeException datatypeException) {
			throw new NotCalculableException(datatypeException);
		}
	}

	public Collection<String> getAvailableCriteria() {
		final List<String> result = new LinkedList<String>();

		for (final Tuple tuple : data) {
			if (tuple.isRelevant()) {
				for (final String string : tuple.getCriteria()) {
					if (!result.contains(string) && !criterionBlacklist.contains(string)) {
						result.add(string);
					}
				}
			}
		}

		return result;
	}

	public void print(PrintStream out) {
		for (final String criterion : getAvailableCriteria()) {
			out.print(criterion + "\t\t");
		}
		out.println("relevance");

		for (final Tuple tuple : data) {
			for (final TupleEntry<?> entry : tuple.getEntries()) {
				out.print(entry.getValue() + "\t\t");
			}

			out.println(tuple.isRelevant());
		}
	}

	public String getName() {
		return name;
	}

	public String getMandant() {
		return mandant;
	}

	public int size() {
		return data.size();
	}

	public void addToBlacklist(String criterion) {
		if (!criterionBlacklist.contains(criterion)) {
			criterionBlacklist.add(criterion);
		}
	}

	public void removeFromBlacklist(String criterion) {
		criterionBlacklist.remove(criterion);
	}
}
