package de.rgse.kuzu.model.datasetview;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

import javax.enterprise.event.Observes;

import de.rgse.kuzu.model.DataSet;
import de.rgse.kuzu.model.Tuple;
import de.rgse.kuzu.model.filter.DataSetFilter;
import de.rgse.kuzu.model.filter.RelevanceDataSetFilter;

public class DataSetView<T> {

	private DataSet dataSet;

	private final Collection<DataSetFilter> filters;

	private final String criterion;

	public DataSetView(DataSet dataSet, String criterion, DataSetFilter... filters) {
		super();
		this.dataSet = dataSet;
		this.filters = new LinkedHashSet<>();
		this.filters.add(new RelevanceDataSetFilter());
		this.filters.addAll(Arrays.asList(filters));
		this.criterion = criterion;
	}

	protected void onDataSetChange(@Observes DataSet dataSet) {
		this.dataSet = dataSet;
	}

	@SuppressWarnings("unchecked")
	public Collection<T> getAll() {
		final Collection<T> result = new ArrayDeque<T>();

		for (final Tuple tuple : getData()) {
			result.add((T) tuple.get(criterion));
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public Collection<T> getAll(String criterion) {
		final Collection<T> result = new ArrayDeque<T>();

		for (final Tuple tuple : getData()) {
			result.add((T) tuple.get(criterion));
		}

		return result;
	}

	public Collection<Tuple> getData() {
		Collection<Tuple> result = dataSet.getData();

		for (final DataSetFilter filter : filters) {
			result = filter.filter(result, criterion);
		}

		return result;
	}

	public void addFilter(DataSetFilter dataSetFilter) {
		filters.add(dataSetFilter);
	}

	public float getCount() {
		return getData().size();
	}

	protected String getCriterion() {
		return criterion;
	}
}
