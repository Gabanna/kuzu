package de.rgse.kuzu.model.datasetview;

import de.rgse.kuzu.model.DataSet;
import de.rgse.kuzu.model.filter.DataSetFilter;

public class TextDataSetView extends DataSetView<String> {

	public TextDataSetView(DataSet dataSet, String criterion, DataSetFilter[] filters) {
		super(dataSet, criterion, filters);
	}

}
