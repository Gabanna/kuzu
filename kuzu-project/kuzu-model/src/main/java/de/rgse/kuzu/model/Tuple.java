package de.rgse.kuzu.model;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.rgse.kuzu.exception.IncompatibleDatatypeException;

public class Tuple {

	private final LinkedHashMap<String, TupleEntry<?>> entries;

	private boolean relevant;

	public Tuple() {
		entries = new LinkedHashMap<String, TupleEntry<?>>();
		relevant = true;
	}

	public Tuple(Map<String, TupleEntry<?>> entries) {
		this.entries = new LinkedHashMap<>(entries);
		relevant = true;
	}

	public String getAsString(String name) throws IncompatibleDatatypeException {
		return get(name, String.class);
	}

	public Float getAsFloat(String name) throws IncompatibleDatatypeException {
		try {
			return get(name, Float.class);

		} catch (final ClassCastException castException) {
			throw new IncompatibleDatatypeException(castException);
		}
	}

	public Boolean getAsBoolean(String name) throws IncompatibleDatatypeException {
		return get(name, Boolean.class);
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String name, Class<T> clazz) throws IncompatibleDatatypeException {
		T result = null;
		final TupleEntry<?> tupleEntry = entries.get(name);
		try {
			final TupleEntry<T> typed = (TupleEntry<T>) tupleEntry;

			if (null != typed) {
				result = typed.getValue();
			}

		} catch (final ClassCastException castException) {
			throw new IncompatibleDatatypeException(clazz, tupleEntry.getValue().getClass());
		}

		return result;
	}

	public Object get(String name) {
		Object result = null;
		final TupleEntry<?> typed = entries.get(name);

		if (null != typed) {
			result = typed.getValue();
		}

		return result;
	}

	public void addEntry(TupleEntry<?> entry) {
		entries.put(entry.getName(), entry);
	}

	public boolean isRelevant() {
		return relevant;
	}

	public void setRelevant(boolean relevant) {
		this.relevant = relevant;
	}

	public List<String> getCriteria() {
		final List<String> criteria = new LinkedList<String>();

		for (final TupleEntry<?> entry : entries.values()) {
			criteria.add(entry.getName());
		}

		return Collections.unmodifiableList(criteria);
	}

	public Collection<TupleEntry<?>> getEntries() {
		return Collections.unmodifiableCollection(entries.values());
	}

	public int size() {
		return entries.size();
	}

	public void print(PrintStream out) {
		for (final TupleEntry<?> entry : entries.values()) {
			System.out.print(entry.getName() + ":" + entry.getValue() + "\t");
		}
		System.out.println();
	}
}
