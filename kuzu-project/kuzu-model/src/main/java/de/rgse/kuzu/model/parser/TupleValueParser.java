package de.rgse.kuzu.model.parser;

import java.util.LinkedList;
import java.util.List;

public abstract class TupleValueParser<OUTPUT> {

	public abstract OUTPUT parse(String input);

	public List<OUTPUT> parse(List<String> input) {
		final List<OUTPUT> result = new LinkedList<OUTPUT>();

		for (final String in : input) {
			result.add(parse(in));
		}

		return result;
	}
}
