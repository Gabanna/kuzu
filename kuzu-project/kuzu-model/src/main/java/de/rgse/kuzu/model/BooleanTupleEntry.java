package de.rgse.kuzu.model;

import de.rgse.kuzu.model.parser.BooleanParser;

public class BooleanTupleEntry extends TupleEntry<Boolean> {

	private static BooleanParser booleanParser = new BooleanParser();

	public BooleanTupleEntry(String criterion) {
		super(criterion);
	}

	public BooleanTupleEntry(String criterion, Boolean value) {
		super(criterion, value);
	}

	@Override
	public void setValue(String value) {
		setValue(booleanParser.parse(value));
	}

}
