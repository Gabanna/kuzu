package de.rgse.kuzu.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import de.rgse.kuzu.calculator.service.DataSetDao;
import de.rgse.kuzu.calculator.util.Current;
import de.rgse.kuzu.model.DataSet;

@Named
@SessionScoped
@SuppressWarnings("serial")
public class DataSetBean implements Serializable {

	private DataSet selectedDataSet;

	private List<DataSet> dataSets;

	@Inject
	private Event<DataSet> dataSetChangedEvent;

	@Inject
	private DataSetDao dataSetDao;

	@Inject
	@Current
	private String currentMandant;

	@PostConstruct
	private void postConstruct() {
		dataSets = dataSetDao.getDataSetsByMandant(currentMandant);
	}

	public void onRowSelect(SelectEvent event) {
		selectedDataSet = (DataSet) event.getObject();
		dataSetChangedEvent.fire(selectedDataSet);
	}

	public List<DataSet> getDataSets() {
		return dataSets;
	}
}
