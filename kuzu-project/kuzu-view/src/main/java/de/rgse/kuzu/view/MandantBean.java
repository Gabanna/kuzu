package de.rgse.kuzu.view;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import de.rgse.kuzu.calculator.events.MandantChangedEvent;
import de.rgse.kuzu.calculator.util.Current;

@Named
@SuppressWarnings("serial")
@SessionScoped
public class MandantBean implements Serializable {

	private String currentMandant;

	@Inject
	private Event<MandantChangedEvent> event;

	@Named("currentMandant")
	@Current
	@Produces
	private String produceMandant() {
		return "tester";
	}

	@Named("availableMandanten")
	@Produces
	private List<String> produceAvailableMandanten() {
		return Arrays.asList("IT-A", "IT-B", "IT-Z");
	}

	public void onSelectMandant(String string) {
		currentMandant = string;
		final MandantChangedEvent mandantChangedEvent = new MandantChangedEvent(currentMandant);
		event.fire(mandantChangedEvent);
	}
}
